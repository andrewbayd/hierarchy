package com.company;

public class NPC extends Character {

    NPC() {
    }

    NPC(String name) {
        super(name);
    }

    @Override
    public void move() {
        System.out.println("I move under AI control");
    }

    void move(Character character) {
        System.out.println("I moving to " + character.getName());
    }

}
