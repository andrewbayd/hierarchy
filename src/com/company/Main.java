package com.company;

public class Main {

    public static void main(String[] args) {
        //create characters and weapons
        Hacker hacker = new Hacker("John");
        Guard guard = new Guard();
        guard.setHealth(100.0);
        Weapon hackerWeapon = Weapon.Gun;
        Weapon guardWeapon = Weapon.MachineGun;

        //characters idle moving
        hacker.move();
        guard.move();
        guard.toGuard();

        //hacker shoot guard
        hacker.shoot(guard, hackerWeapon);
        System.out.println("Guard health after shooting: " + guard.getHealth());

        //guard starts to following hacker
        guard.move(hacker);

        //guard shoot hacker
        guard.shoot(hacker, guardWeapon);
        System.out.println("Hacker health after shooting: " + hacker.getHealth());

        //hacker found a medicine box
        hacker.heal();
        System.out.println("Hacker health after healing: " + hacker.getHealth());

        //hacker use his super skill
        if (hacker.isSuperSkillAvailable()) {
            hacker.useSuperSkill();
        }
    }
}