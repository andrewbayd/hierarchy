package com.company;

public enum Weapon {
    Knife(5.0),
    Gun(20.0),
    MachineGun(35.0);

    double power;

    Weapon(double power) {
        this.power = power;
    }

    public double getPower() {
        return power;
    }
}
