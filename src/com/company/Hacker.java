package com.company;

public class Hacker extends PlayableCharacter implements SuperSkills {

    public Hacker() {
    }

    Hacker(String name) {
        super(name);
    }

    @Override
    public void useSuperSkill() {
        System.out.println("I've hacked all things!");
    }
}
