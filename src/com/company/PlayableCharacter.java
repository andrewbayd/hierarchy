package com.company;

public class PlayableCharacter extends Character {

    private final boolean hasSuperSkill = true;

    PlayableCharacter() {
    }

    PlayableCharacter(String name) {
        super(name);
    }

    boolean isSuperSkillAvailable() {
        return hasSuperSkill;
    }

    @Override
    public void move() {
        System.out.println("I move under player control");
    }

    void heal() {
        this.setHealth(100.0);
    }
}
