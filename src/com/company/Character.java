package com.company;

public abstract class Character {

    private String name;
    private double health;

    Character() {
    }

    Character(String name) {
        this.name = name;
        this.health = 100.0;
    }

    String getName() {
        return name;
    }

    double getHealth() {
        return health;
    }

    void setHealth(double health) {
        this.health = health;
    }

    void shoot(Character character, Weapon weapon) {
        character.setHealth(character.getHealth() - weapon.getPower());
    }

    public abstract void move();

}
